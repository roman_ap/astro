# This is a Python script to run the RK4 integrator from the "th_body"
# subroutine in fortran file 'integ.f90'. Outputs are saved in files
# 'data1.dat', 'data2.dat' and 'data3.dat' 

# Improvement: maybe adding the mass of the bodies as input parameters.

import integ

h     = 1.e-1       # RK4 time-step
steps = 10000000    # number of steps

integ.th_body(h, steps)