"""
Created on Mon Mar 22 00:38:42 2021
@author: oveis
"""
import matplotlib.pyplot as plt
import numpy as np

data1 = np.loadtxt("./data1.dat")
Time= data1[:,0]
x1  = data1[:,1]
y1  = data1[:,2]
z1  = data1[:,3]
Vx1 = data1[:,4]
Vy1 = data1[:,5]
Vz1 = data1[:,6]
R1  = data1[:,7]
V1  = data1[:,8]
Energy = data1[:,9]

data2= np.loadtxt("./data2.dat")
x2   = data2[:,1]
y2   = data2[:,2]
z2   = data2[:,3]
Vx2  = data2[:,4]
Vy2  = data2[:,5]
Vz2  = data2[:,6]
R2   = data2[:,7]
V2   = data2[:,8]

# data3 = np.loadtxt("./data3.dat")
# x3   = data3[:,1]
# y3   = data3[:,2]
# z3   = data3[:,3]
# Vx3  = data3[:,4]
# Vy3  = data3[:,5]
# Vz3  = data3[:,6]
# R3   = data3[:,7]
# V3   = data3[:,8]


fig1 = plt.figure()
ax1  = plt.subplot()
# ax1.set_title("Mimas' Position vector") 
ax1.set_ylabel('x')
ax1.set_xlabel('Time')
ax1.plot(Time,x1, c='r', label='x1')
ax1.plot(Time,x2, c='b', label='x2')
# ax1.plot(Time,x3, c='y', label='x3')
ax1.legend(loc="best", shadow=True)
plt.grid()


fig2 = plt.figure()
ax2  = plt.subplot()
ax2.set_title("Tethys' position vector")
ax2.set_ylabel('Position') 
ax2.set_xlabel('Time')
ax2.plot(Time, y1, c='r', label='y1')
ax2.plot(Time, y2, c='b', label='y2')
# ax2.plot(Time, y3, c='y', label='y3')
ax2.legend(loc="best", shadow=True)
plt.grid()


fig3 = plt.figure()
ax3  = plt.subplot()
ax3.set_title("z")
ax3.set_ylabel('Position')
ax3.set_xlabel('Time')
ax3.plot(Time, z1, c='r', label='z1')
ax3.plot(Time, z2, c='b', label='z2')
# ax3.plot(Time, z3, c='y', label='z3')
ax3.legend(loc="best", shadow=True)
plt.grid()


fig4 = plt.figure()
ax4  = plt.subplot()
ax4.set_title('Position vectors of the 3 sattelites wrt Time using RK4') 
ax4.set_ylabel('Position')
ax4.set_xlabel('Time')
ax4.plot(Time, R1, label='Mimas')
ax4.plot(Time, R2, label='Tethys')
# ax4.plot(Time, R3, label='Titan')
ax4.legend(loc="best", shadow=True)
plt.grid()


fig5 = plt.figure()
ax5  = plt.subplot()
ax5.set_title('Velocity vectors of the three sattelites wrt Time using RK4') 
ax5.set_ylabel('Velocity') 
ax5.set_xlabel('Time')
ax5.plot(Time, V1, label='Mimas')
ax5.plot(Time, V2, label='Tethys')
# ax5.plot(Time, V3, label='Titan')
ax5.legend(loc="best", shadow=True)
plt.grid()


fig6 = plt.figure()
ax6  = plt.subplot()
ax6.set_title('Energy (Step-size: $\mathregular{10^{-4}}$, Steps: $\mathregular{10^9}$)')
ax6.set_ylabel('Energy')
ax6.set_xlabel('Time step')
ax6.plot(Time,Energy)
plt.grid()

# # set the axis line width in pixels
# for axis in 'left', 'bottom':
#     ax6.spines[axis].set_linewidth(2.5)
# ax6.tick_params(axis='both', which='major', labelsize=15, width=2.5, length=10)


plt.show()
