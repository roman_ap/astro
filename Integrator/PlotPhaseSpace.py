"""
Created on Mon Mar 22 00:38:42 2021
@author: oveis
"""
import matplotlib.pyplot as plt
import numpy as np
# from mpl_toolkits import mplot3d

body1 = np.loadtxt("./data1.dat")
body2 = np.loadtxt("./data2.dat")
body3 = np.loadtxt("./data3.dat")

# Time = data[700:,0]
Time = body1[:,0]

fig1 = plt.figure()
ax1 = plt.axes(projection='3d')

x1 = body1[:,1]
y1 = body1[:,2]
z1 = body1[:,3]

x2 = body2[:,1]
y2 = body2[:,2]
z2 = body2[:,3]

x3 = body3[:,1]
y3 = body3[:,2]
z3 = body3[:,3]

ax1.plot3D(x1, y1, z1, c='r', linewidth=0.3, label='Mimas')
ax1.plot3D(x2, y2, z2, c='b', linewidth=0.3, label='Tethys')
ax1.plot3D(x3, y3, z3, c='y', linewidth=0.3, label='Titan')
ax1.set_title('Orbits using RK4') 

ax1.set_xlabel('x')
ax1.set_ylabel('y') 
ax1.set_zlabel('z') 

ax1.legend(loc="best", shadow=True)
plt.grid()

# plt.savefig('plot2.pdf')

# fig2 = plt.figure()
# ax2  = plt.subplot()
# ax2.set_title('Expression levels by RK4 (repressive regulations)') 
# ax2.set_ylabel('P(i)') 
# ax2.set_xlabel('Time')

# ax2.plot(Time,P1, c='r', label='P1')
# ax2.plot(Time,P2, c='b', label='P2')
# ax2.plot(Time,P3, c='y', label='P3')

# ax2.legend(loc="best", shadow=True)
# plt.grid()


plt.show()
