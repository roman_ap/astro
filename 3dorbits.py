import numpy as np
import matplotlib.pyplot as plt 
from scipy.signal import argrelextrema
import re

"""
This code is maybe very long but it's just because I'm a bad programer.
It just plots, the trajectories, velocities, energy and compare the trajectory
with the real one

Even though we use the preturbated simulation with a very small time step we will
still see some error in the orbit of Titan due to interaction with the sun.
"""


"""
Getting the data from the files
"""
nstep=3*10**6
dt =1 #seconds
print_step = 1
data1 = np.loadtxt('data1.dat')
data2 = np.loadtxt('data2.dat')
data3 = np.loadtxt('data3.dat')
data4 = np.loadtxt('data4.dat') #for the energies
au = 149597870700 #astronomical units
jour = 24*3600 #dias


time = data1[:,0]/jour
x1 = data1[:,1]/au
y1 = data1[:,2]/au
z1 = data1[:,3]/au
vx1 = data1[:,1]/au*jour
vy1 = data1[:,2]/au*jour
vz1 = data1[:,3]/au*jour

x2 = data2[:,1]/au
y2 = data2[:,2]/au
z2 = data2[:,3]/au
vx2 = data2[:,1]/au*jour
vy2 = data2[:,2]/au*jour
vz2 = data2[:,3]/au*jour


x3 = data3[:,1]/au
y3 = data3[:,2]/au
z3 = data3[:,3]/au
vx3 = data3[:,1]/au*jour
vy3 = data3[:,2]/au*jour
vz3 = data3[:,3]/au*jour

energy = data4[:,1]
"""
Ploting the 3d trajectory
"""
ax = plt.figure().add_subplot(projection='3d')
ax.plot(x1, y1, z1, label = 'Mimas')
ax.plot(x2, y2, z2, label = 'Thetis')
ax.plot(x3, y3, z3, label = 'Titan')
ax.legend()
ax.set_ylabel('$X$')
ax.set_ylabel('$Y$')
ax.set_ylabel('$Z$')

"""
Ploting the temporal evolution of x, y, x
"""
fig, axs = plt.subplots(3, sharex=True, sharey=True)
fig.suptitle('Trajectories')
axs[0].plot(time, x1, time, x2, time, x3)
axs[1].plot(time, y1, time, y2, time, y3)
axs[2].plot(time, z1, time, z2, time, z3)
axs[0].set_ylabel('$X$')
axs[1].set_ylabel('$Y$')
axs[2].set_ylabel('$Z$')
fig.supxlabel('time [days]')
fig.supylabel('distance [au]')

"""
Ploting the temporal evolution of the velocities
"""
fig1, axs1 = plt.subplots(3)
fig1.suptitle('Velocities')
axs1[0].plot(time, vx1, time, vx2, time, vx3)
axs1[1].plot(time, vy1, time, vy2, time, vy3)
axs1[2].plot(time, vz1, time, vz2, time, vz3)
axs1[0].set_ylabel('$X$')
axs1[1].set_ylabel('$Y$')
axs1[2].set_ylabel('$Z$')
fig1.supxlabel('time [days]')
fig1.supylabel('distance [au]')

"""
Period calculation
"""
#I compute the distance between maximums and I do the average between them
#Since we acumulate error with the time, maybe I should take the first maximums and not all
#Anyway, I think this doesn't affect the result almost nothing...
def period(time,x1):
    peaks = np.where((x1[1:-1] > x1[0:-2]) * (x1[1:-1] > x1[2:]))[0] + 1
    periodo = time[peaks]
    p = []
    for i in range(len(periodo)-1):
        p.append(periodo[i+1]-periodo[i])
    return p
                
T1 = np.mean(period(time,x1))
T2 = np.mean(period(time,x2))
T3 = np.mean(period(time,x3))
T1err = np.std(period(time,x1))
T2err = np.std(period(time,x2))
T3err = np.std(period(time,x3))
[T1real, T2real, T3real] = [0.9424218, 1.888, 15.945421]
print('period Mimas : ', T1,'+-',T1err, 'days')
print('period Thetis : ', T2,'+-',T2err, 'days')
print('period Titan : ', T3,'+-',T3err, 'days')
print('Real periods are : ', T1real, T2real, T3real)
#%matplotlib

"""
Energy
We expect some small oscillations and the energy tends to decrease.
However, those oscillations are very small in comapare with the total energy,
and this decreasing process is due to the time step, because you can check that 
if you reduce the time step you also reduce this variation in the energy.
In conclusion: Even if you observe variations, don't worry those are only in 
the last decimals, good job!
"""
plt.figure()
plt.plot(time, energy)
plt.title('Total energy for all bodies')
plt.xlabel('time [days]')
plt.ylabel('Energy [J]') #Should I use another unit??

"""
Real data from the text files. You need to have mimas.txt, tethis.txt and
titan.txt in the same repository as the code
"""
def reading(file = 'mimas.txt'):
    file = open(file, 'r')
    lines = file.readlines()
    count = 0
    # Strips the newline character
    x = []
    y = []
    z = []
    vx = []
    vy = []
    vz = []
    for line in lines:
        #print(line)
        count += 1
        if line.startswith(' X'):
            data = re.findall(r'-?\d+\.{0,1}\d*', line)
            x.append(float(data[0])*10**float(data[1])*1000/au)
            y.append(float(data[2])*10**float(data[3])*1000/au)
            z.append(float(data[4])*10**float(data[5])*1000/au)
        elif line.startswith(' VX'):
            data1 = re.findall(r'-?\d+\.{0,1}\d*', line)
            vx.append(float(data1[0])*10**float(data1[1])*1000/au)
            vy.append(float(data1[2])*10**float(data1[3])*1000/au)
            vz.append(float(data1[4])*10**float(data1[5])*1000/au)
    return x, y, z, vx, vy, vz

"""
Read the real data and plot it
"""
[x_mimas, y_mimas, z_mimas, vx_mimas, vy_mimas, vz_mimas] = reading('mimas.txt')
[x_tethis, y_tethis, z_tethis, vx_tethis, vy_tethis, vz_tethis] = reading('tethis.txt')
[x_titan, y_titan, z_titan, vx_titan, vy_titan, vz_titan] = reading('titan.txt')
time_real = np.arange(len(x_mimas)) #we have one data per day

#Plot of the real trajectories
fig_real, axs_real = plt.subplots(3, sharex=True, sharey=True)
fig_real.suptitle('Real trajectories')
axs_real[0].plot(time_real, x_mimas,'*', time_real, x_tethis,'+', time_real, x_titan,'o')
axs_real[1].plot(time_real, y_mimas,'*', time_real, y_tethis,'+', time_real, y_titan,'o')
axs_real[2].plot(time_real, z_mimas,'*', time_real, z_tethis,'+', time_real, z_titan,'o')
axs_real[0].set_ylabel('$X$')
axs_real[1].set_ylabel('$Y$')
axs_real[2].set_ylabel('$Z$')
fig_real.supxlabel('time [days]')
fig_real.supylabel('distance [au]')

#Plot of the real velocities
fig_vreal, axs_vreal = plt.subplots(3, sharex=True, sharey=True)
fig_vreal.suptitle('Real velocities')
axs_vreal[0].plot(time_real, vx_mimas,'*', time_real, vx_tethis,'+', time_real, vx_titan,'o')
axs_vreal[1].plot(time_real, vy_mimas,'*', time_real, vy_tethis,'+', time_real, vy_titan,'o')
axs_vreal[2].plot(time_real, vz_mimas,'*', time_real, vz_tethis,'+', time_real, vz_titan,'o')
axs_vreal[0].set_ylabel('$X$')
axs_vreal[1].set_ylabel('$Y$')
axs_vreal[2].set_ylabel('$Z$')
fig_vreal.supxlabel('time [days]')
fig_vreal.supylabel('velocity [au/days]')

"""
Comparision between real and computed trajectory
"""
def compara(time_real, x_real, y_real, z_real, time, x, y, z, name = 'the moon', magnitude = 'trajectory'):
    #This function can be used to work with velocities I think
    #But maybe plot velocities is useless, IDK
    #plots with the trajectories
    fig_compare, axs_compare = plt.subplots(3, sharex=True, sharey=True)
    fig_compare.suptitle('Real vs computed ' + magnitude +' of '+name)
    axs_compare[0].plot(time_real, x_real,'*', time, x)
    axs_compare[1].plot(time_real, y_real,'*', time, y)
    axs_compare[2].plot(time_real, z_real,'*', time, z)
    axs_compare[0].set_ylabel('$X$')
    axs_compare[1].set_ylabel('$Y$')
    axs_compare[2].set_ylabel('$Z$')
    fig_compare.supxlabel('time [days]')
    fig_compare.supylabel('distance [au]')
    #plots with the errors
    #I interpolate the position at the exact time of the measurement
    errx = np.abs( x_real - np.interp(time_real, time, x))
    erry = np.abs( y_real - np.interp(time_real, time, y))
    errz = np.abs( z_real - np.interp(time_real, time, z))
    errdistance = (errx**2+erry**2+errz**2)**0.5
    #First element fails, not a big deal, but I deleted it
    fig_err, axs_err = plt.subplots(1, sharex=True, sharey=True)
    fig_err.suptitle('Error in the calculated trajectory of '+name)
    axs_err.bar(time_real[1:], errdistance[1:])
    fig_err.supxlabel('time [days]')
    fig_err.supylabel('error [au]')
    

    

index = np.argmin(np.abs(np.array(time)-time_real[-1]))
time_compara = time[0:index+1]
compara(time_real, x_titan, y_titan, z_titan, time_compara, x3[0:index+1], y3[0:index+1], z3[0:index+1], name = 'Titan')
compara(time_real, x_tethis, y_tethis, z_tethis, time_compara, x2[0:index+1], y2[0:index+1], z2[0:index+1], name = 'Tethis')
compara(time_real, x_mimas, y_mimas, z_mimas, time_compara, x1[0:index+1], y1[0:index+1], z1[0:index+1], name = 'Mimas')
"""
%matplotlib
After that do the 2 falling body problem using initial velocity 0, plot the 
temporal evolution of the energy and how the bodies approach with respect to time.
"""