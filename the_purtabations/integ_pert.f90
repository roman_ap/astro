! This code integrates the orbits of the the Saturn system with three of its satellites, Mimas, Tethys, and Titan.
! Saturn is supposed to be at the center.
! M1: Mimas
! M2: Tethys
! M3: Titan

program three_body_RK4
    implicit none

    integer, parameter :: dp = SELECTED_REAL_KIND(P=8)

    ! Math constants
    real (KIND=dp), parameter :: pi = 3.1415926535897932384_dp
    real (KIND=dp), parameter :: G  = 6.674e-11_dp  
    real (KIND=dp), parameter :: M  = 568.32e24_dp              ! Saturn's mass [kg]
    real (KIND=dp), parameter :: GM = G*M                       ! Saturn's gravitational constant [m^3/kg/s^2]
    
    ! RK4 constants and variables
    real (KIND=dp), dimension(3,6) :: k0, k1, k2, k3, k4
    real (KIND=dp), parameter      :: h = 1.e-1_dp               !RK4 time-step size [s]
    integer, parameter             :: Npts = 1.e8               !Number of steps

    ! Declaration of sattelites' variables
    real (KIND=dp), allocatable :: R(:,:), V(:,:)               !1st dimension: 3 bodies, 2nd dimension: 3 coordinates
    real (kind=dp), allocatable :: R_norm(:), V_norm(:)         !Norm of R and V for each body
    real (KIND=dp)              :: GM1, GM2, GM3, M1, M2, M3
    real (KIND=dp)              :: t, energy

    ! Other variables
    integer :: i, j, k!, body_num!, codeError, io


! ==================== Initial Condition =====================================
    t = 0.e0_dp

    allocate(R(3,3), V(3,3), R_norm(3) , V_norm(3))

    ! Initial positions of Mimas, Tethys, and Titan [m]
    R(1,1) = -1.155743126780157e8_dp
    R(1,2) =  1.325405469178612e8_dp
    R(1,3) = -6.405744636561111e7_dp

    R(2,1) =  2.736338967931043e7_dp
    R(2,2) = -2.583656101498802e8_dp
    R(2,3) =  1.389825768439401e8_dp

    R(3,1) =  1.022946170421365e9_dp
    R(3,2) =  5.037897354451453e8_dp 
    R(3,3) = -3.617275927456587e8_dp

    R_norm(:) = norm2(R(:,:), dim=2)

    ! Initial velocities of Mimas, Tethys, and Titan [m/s]
    V(1,1) = -1.127495088210949e4_dp
    V(1,2) = -7.155743126780157e3_dp
    V(1,3) =  4.806656698122779e3_dp

    V(2,1) =  1.126007562976311e4_dp 
    V(2,2) =  4.553114846840171e2_dp 
    V(2,3) = -1.372033455417533e3_dp
 
    V(3,1) = -2.810756941076592e3_dp 
    V(3,2) =  4.506080188745751e3_dp 
    V(3,3) = -2.043514745714397e3_dp

    V_norm(:) = norm2(V(:,:), dim=2)

    M1 = 3.75e19_dp   ! Mimas' mass [kg]
    M2 = 61.76e19_dp  ! Tethys' mass [kg]
    M3 = 1.3455e23_dp ! Titan's mass [kg]
    
    GM1 = GM*M1/M    
    GM2 = GM*M2/M    
    GM3 = GM*M3/M    
    
! ==================== RK4 implementation =====================================

    open(unit=11, file='data1.dat')
    open(unit=12, file='data2.dat')
    open(unit=13, file='data3.dat')

    write(11,*) "#  0.Time  1.x1  2.y1  3.z1  4.Vx1  5.Vy1  6.Vz1  7.R1  8.V1  9.energy"
    write(12,*) "#  0.Time  1.x2  2.y2  3.z2  4.Vx2  5.Vy2  6.Vz2  7.R2  8.V2"
    write(13,*) "#  0.Time  1.x3  2.y3  3.z3  4.Vx3  5.Vy3  6.Vz3  7.R3  8.V3"

    ! Initialization of k-values
    k0(:,:) = 0.e0_dp    ! This table of zeros will be used only to make k1.

    ! main loop
    do i=1, Npts-1
        call k_sub(V(:,:), R(:,:),         k0(:,:), k1(:,:))    ! k1 for all sattelites
        call k_sub(V(:,:), R(:,:),         k1(:,:), k2(:,:))    ! k2 for all sattelites
        call k_sub(V(:,:), R(:,:),         k2(:,:), k3(:,:))    ! k3 for all sattelites
        call k_sub(V(:,:), R(:,:), 2.e0_dp*k3(:,:), k4(:,:))    ! k4 for all sattelites

        ! update of position and velocity
        do j=1, 3
            do k=1, 3
                R(j,k) = R(j,k) + ( h * (k1(j,k)   + 2.e0_dp*k2(j,k)   + 2.e0_dp*k3(j,k)   + k4(j,k)   ) )/6.e0_dp
                V(j,k) = V(j,k) + ( h * (k1(j,k+3) + 2.e0_dp*k2(j,k+3) + 2.e0_dp*k3(j,k+3) + k4(j,k+3) ) )/6.e0_dp
            end do
        end do

        ! Position and velocity magnitudes for the 3 sattelites
        R_norm(:) = norm2(R(:,:), dim=2)
        V_norm(:) = norm2(V(:,:), dim=2)

        ! Update of time
        t = t+h
        
        ! Energy of the system
        energy = 0.5* M1*V_norm(1)**2 + 0.5*M2*V_norm(2)**2 + 0.5*M3*V_norm(3)**2 &
                 -GM*(M1/R_norm(1) + M2/R_norm(2) + M3/R_norm(3)) &
                 -GM1*M2/sqrt((R(1,1)-R(2,1))**2 + (R(1,2)-R(2,2))**2 + (R(1,3)-R(2,3))**2) &
                 -GM1*M3/sqrt((R(1,1)-R(3,1))**2 + (R(1,2)-R(3,2))**2 + (R(1,3)-R(3,3))**2) &
                 -GM2*M3/sqrt((R(2,1)-R(3,1))**2 + (R(2,2)-R(3,2))**2 + (R(2,3)-R(3,3))**2)

        ! Print data to files
        if (MOD(i,100000)==0) then      ! Print data every 100000 steps
            write(11,*) t, R(1,1), R(1,2), R(1,3), V(1,1), V(1,2), V(1,3), R_norm(1), V_norm(1), energy
            write(12,*) t, R(2,1), R(2,2), R(2,3), V(2,1), V(2,2), V(2,3), R_norm(2), V_norm(2)
            write(13,*) t, R(3,1), R(3,2), R(3,3), V(3,1), V(3,2), V(3,3), R_norm(3), V_norm(3)
        end if
    end do

    close(11)
    close(12)
    close(13)
    deallocate(R, V, R_norm, V_norm)

contains

! ==================== Subroutine: K =========================================
    ! This subroutine produces 6 "k"s for each body
    subroutine k_sub(VV, RR, k_in, k_out)
        implicit none

        integer, parameter :: nbody = 3     ! number of bodies
        real (KIND=dp), dimension(nbody,3), intent(in) :: RR, VV
        real (KIND=dp), dimension(nbody,3)             :: R_vec2
        real (kind=dp), dimension(nbody)               :: RR_norm
        real (kind=dp), dimension(nbody)               :: d_12, d_13, d_23
        real (KIND=dp), dimension(nbody,6), intent(in) :: k_in
        real (KIND=dp), dimension(nbody,6), intent(out):: k_out       !vector k with 6 components
        INTEGER                                    :: ii, jj

        ! Making k(i,j) for each body. For each body we need to compute 6 k values (j=1:6)
        ! i: the body number (1 for Mimas, 2 for Tethys and 3 for Titan), j=1:3 the , j=4:6 is the velocity vector.

        ! k(:,1:3)
        ! For k1(1:3), by putting a zero array instead of k_in the 2nd term on the rhs vanishes.
        do jj=1, 3
            k_out(1,jj) = VV(1,jj) + h*k_in(1,jj+3)/2.e0_dp
            k_out(2,jj) = VV(2,jj) + h*k_in(2,jj+3)/2.e0_dp
            k_out(3,jj) = VV(3,jj) + h*k_in(3,jj+3)/2.e0_dp
        end do

        ! k(:,4:6)

        ! This loop is funny! Only makes the position vectors to be used for making k2(1:3), k3(1:2), k4(1:2).
        ! So, not a big deal although it might seem crazy why I did this way :D
        do ii=1, 3
            R_vec2(1,ii) = RR(1,ii) + h*k_in(1,ii)/2.e0_dp
            R_vec2(2,ii) = RR(2,ii) + h*k_in(2,ii)/2.e0_dp
            R_vec2(3,ii) = RR(3,ii) + h*k_in(3,ii)/2.e0_dp
            
            d_12(ii) = R_vec2(2,ii) - R_vec2(1,ii)
            d_13(ii) = R_vec2(3,ii) - R_vec2(1,ii)
            d_23(ii) = R_vec2(3,ii) - R_vec2(2,ii)
        end do

        ! The norm of the 3 position vectors to be used in the denominators of k
        RR_norm(:) = norm2(R_vec2, dim=2)

        do jj=4, 6
            k_out(1,jj) = - GM*R_vec2(1,jj-3)/ (R_norm(1))**3 &
                          + GM2*d_12(jj-3) / (norm2(d_12))**3 &
                          + GM3*d_13(jj-3) / (norm2(d_13))**3
                          
            k_out(2,jj) = - GM*R_vec2(2,jj-3)/ (R_norm(2))**3 &
                          + GM1*d_12(jj-3) / (norm2(d_12))**3 &
                          + GM3*d_23(jj-3) / (norm2(d_23))**3

            k_out(3,jj) = - GM*R_vec2(3,jj-3)/ (R_norm(3))**3 &
                          + GM1*d_13(jj-3) / (norm2(d_13))**3 &
                          + GM2*d_23(jj-3) / (norm2(d_23))**3
        end do

    end subroutine k_sub

end program three_body_RK4
